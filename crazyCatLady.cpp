///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author  @Patrick Manuel <@pamanuel@hawaii.edu>
/// @date    @01_15_2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
using namespace std;

int main( int argc, char* argv[] ) {
	cout << "Oooooh! " << argv[1]  << " you’re so cute!" << endl ;
        return 0;
}
